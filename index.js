const CronJob = require("cron").CronJob
require("dotenv").config()

const post = require("./post")
const scrape = require("./scrape")
const urls = require("./urls")

function scrapeAndPost(url) {
  console.log(`>>> Scraping ${url}`, new Date())
  return () => {
    scrape(url)
      .then(post)
      .then(() => {
        console.log(`>>> Successfully scraped ${url}`)
      })
      .catch(err => {
        console.error(`>>> Failed to scrape ${url}`)
      })
  }
}

urls.forEach(u => {
  const url = `https://www.vesselfinder.com/vessels/${u}`
  const job = new CronJob({
    cronTime: process.env.CRONTIME,
    onTick: scrapeAndPost(url),
    timeZone: "America/New_York"
  })

  job.start()
})
