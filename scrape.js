const cheerio = require("cheerio")
const http = require("axios")

module.exports = function scrapeLocation(url) {
  const config = {
    method: "GET",
    headers: {
      "User-Agent":
        "Mozilla / 5.0(Macintosh; Intel Mac OS X 10_13_3) AppleWebKit / 537.36(KHTML, like Gecko) Chrome / 67.0.3396.99 Safari / 537.36"
    },
    url
  }
  return http(config).then(data => {
    const $ = cheerio.load(data.data)

    const scraped = {
      coordinates: null,
      lastReport: null,
      url
    }

    $(".column.ship-section table tr").each((i, el) => {
      const $el = $(el)
      const keyTd = $el.find("td").get(0)
      const $keyTd = $(keyTd)
      const key = $keyTd.text()

      const valueTd = $el.find("td").get(1)
      const $valueTd = $(valueTd)
      const value = $valueTd.text()

      if (key.toLowerCase().includes("coordinates")) {
        scraped.coordinates = value
      } else if (key.toLowerCase().includes("last report")) {
        scraped.lastReport = value
      }
    })

    return scraped
  })
}
