# Sea vessel location tracker

Scrapes ship location data from VesselFinder.com.

## Usage
Set desired `CRONTIME` in `.env` and either run `npm start` or `pm2 start` if you are using that tool.

The scaper will pull all location data for vessels in `urls.js` and save the data to [this Google spreadsheet](https://docs.google.com/spreadsheets/d/11sL7nKV0jYCoJTs5iqdsz5tB4RA4G9swgCt2ZW5sOiE/edit?usp=sharing).