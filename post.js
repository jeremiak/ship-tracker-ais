const querystring = require("querystring")

const http = require("axios")

const formId = "1FAIpQLSe8N0nSWmrHi6daK9Z9C7LWWJVL4xXw0pQ30lTho5tZjmWu0w"
const formUrl = `https://docs.google.com/forms/d/e/${formId}/formResponse`

module.exports = function postToGoogleSheet(scraped) {
  const data = {
    "entry.890994157": scraped.url,
    "entry.1759892057": scraped.coordinates,
    "entry.667382804": scraped.lastReport,
    fbzx: "2920242039646484642",
    fvv: "1"
  }
  const config = {
    method: "POST",
    headers: {
      "Content-Type": "application/x-www-form-urlencoded"
    },
    url: formUrl,
    data: querystring.stringify(data)
  }
  return http(config)
}
